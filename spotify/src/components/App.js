import React from 'react';
import {
    BrowserRouter as Router,
    Link,
    Switch,
    Route
} from 'react-router-dom'

import Loading from './Loading'
import GetStarted from './GetStarted'
import ChooseMode from './ChooseMode'

import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        <Router>
            <div>
                <nav>
                <ul>
                    <li>
                        <Link to="/">Loading</Link>
                    </li>
                    <li>
                        <Link to="/get-started">GetStarted</Link>
                    </li>
                    <li>
                        <Link to="/choose-mode">ChooseMode</Link>
                    </li>
                </ul>
                </nav>

                <Switch>
                <Route path="/get-started">
                    <GetStarted />
                </Route>
                <Route path="/choose-mode">
                    <ChooseMode />
                </Route>
                <Route path="/">
                    <Loading />
                </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;