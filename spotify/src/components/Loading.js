import React from "react";
import Link from 'react-router-dom';
import Logo from './Logo';

class Loading extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="loading">
                <Logo />
            </div>
        );
    }
}
export default Loading;
