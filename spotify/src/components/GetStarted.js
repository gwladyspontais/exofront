import React from "react";
import Link from 'react-router-dom';
import Button from 'react-bootstrap/Button';

import Logo from './Logo.js';

class GetStarted extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="container-getstarted-body">
                <div class="logo">
                    <Logo />
                </div>
                <div>
                    <div class="text">
                        <h1 class="title">Enjoy listening to music</h1>
                        <p class="greyText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sagittis enim purus sed phasellus. Cursus ornare id scelerisque aliquam.</p>
                    </div>
                    <div class="container-button-lg">
                        <Button href="./choose-mode" size="lg">Get started</Button>{' '}
                    </div>
                </div>
            </div>
        );
    }
}
export default GetStarted;
