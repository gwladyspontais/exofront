import React from "react";
import Link from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Logo from './Logo';

import moon from '../images/moon.png';
import sun from '../images/sun.png';

class ChooseMode extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="container-choosemode-body">
                <div class="logo">
                    <Logo />
                </div>
                <div>
                    <div class="text">
                        <h1 class="title">Choose Mode</h1>
                    </div>
                    <div class="row-mode">
                        <div class="modeButton">
                            <img class="img-mode" src={moon}></img>
                            <p class="text-mode">Dark Mode</p>
                        </div>
                        <div class="modeButton">
                            <img class="img-mode" src={sun}></img>
                            <p class="text-mode">Light Mode</p>
                        </div>
                    </div>
                    <div class="container-button-lg">
                        <Button href="#" size="lg">Continue</Button>{' '}
                    </div>
                </div>
            </div>
        );
    }
}
export default ChooseMode;
