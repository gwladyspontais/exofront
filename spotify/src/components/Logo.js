import React from "react";
import logo from '../images/logo.png';

class Logo extends React.Component {
    render() {
        return (
            <div>
                <img src={logo}></img>
            </div>
        );
    }
}
export default Logo;
