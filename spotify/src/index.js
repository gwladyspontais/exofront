import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import GetStarted from './components/GetStarted';
import ChooseMode from './components/ChooseMode';

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
);
